import tkinter as tk
from constants import *
from tkinter import messagebox as mb
from tkinter import simpledialog as sd
from tkinter import font

# * Cac toa tau la cac rectangle. Luoi them hinh vao


class GUI:
    # Init class
    def __init__(self) -> None:
        # Tao window root cua tk
        self.root = tk.Tk()
        self.root.title("Array Demo")
        # Dat kich thuoc va vi tri cua window
        self.root.geometry(
            f"{WIN_WIDTH}x{WIN_HEIGHT}+{int(self.root.winfo_screenwidth()/2 - WIN_WIDTH/2)}+"
            f"{int(self.root.winfo_screenheight()/2 - WIN_HEIGHT/2)}")
        # Khong cho phep resize (thay doi kich thuoc)
        self.root.resizable(width=False, height=False)

        # Cai dat bien
        self.font = font.Font(family="Times", size=16)
        self.array = []
        self.operations = []
        self.rects = []
        self.texts = []

        # Goi function
        self.init_widget()
        self.draw_widget()
        self.init_elements("0")

        # Mainloop. Khong co dong nay tk se khong hien len
        self.root.mainloop()

    def init_widget(self):
        # Canvas. Cac method create_rectangle, create_text thuoc class nay
        self.mainframe = tk.Canvas(self.root)
        # Nut nhan bind vao 1 function (bang parameter command. Font lay ten font hoac ten family)
        self.add_button = tk.Button(self.mainframe, text="Add", font=(
            "times", 16, "bold"), command=self.add_element, bg="#03fc41", fg="#FFFFFF")
        self.remove_button = tk.Button(self.mainframe, text="Remove", font=(
            "times", 16, "bold"), command=self.remove_element, bg="#c92020", fg="#FFFFFF")
        self.complete_button = tk.Button(self.mainframe, text="Complete", font=(
            "times", 16, "bold"), command=self.complete, bg="#ccc316", fg="#FFFFFF")
        # Trong canvas set scrollbar xong phai set scrollregion. Cai nay se set o duoi khi so o vuot qua width
        self.mainframe_scroll_x = tk.Scrollbar(
            self.root, command=self.mainframe.xview, orient="horizontal")
        self.mainframe.config(xscrollcommand=self.mainframe_scroll_x.set)

    def draw_widget(self):
        # Dat widget len tk. X, Y, width, height hoat dong nhu trong scratch
        self.mainframe.place(x=0, y=0, width=WIN_WIDTH, height=WIN_HEIGHT-20)
        self.add_button.place(x=20, y=WIN_HEIGHT-80, width=100, height=50)
        self.remove_button.place(x=250, y=WIN_HEIGHT-80, width=100, height=50)
        self.complete_button.place(
            x=480, y=WIN_HEIGHT-80, width=100, height=50)
        self.mainframe_scroll_x.place(x=0, y=100, width=600, height=30)

    def init_elements(self, text):
        # Dat toa tau dau tien vao
        self.rects.append(self.mainframe.create_rectangle(
            20, 20, RECT_WIDTH, RECT_HEIGHT))
        if len(text) > 8:
            mb.showinfo(
                "Info", f"String length > 8. Discarded all character after {text[7]}")
            text = text[:4] + "\n" + text[4:8]
        if 4 < len(text) <= 8:
            text = text[:4] + "\n" + text[4:]
        self.texts.append(self.mainframe.create_text(
            50, 50, text=text, font=("times", 20)))
        self.array.append(text)

    def add_element(self):
        # Function trong function. Bat dau doc tu dong 166
        def insert_set(insert_pos):
            # Neu user khong nhap gi vao entry, set insert_pos vao cuoi array
            if not insert_pos.get():
                insert_pos.set(len(self.array))
            # Neu xoa het phan tu trong array roi
            if len(self.array) == 0:
                insert_val = sd.askstring("Value", "Enter a value to insert")
                if not insert_val:
                    insert_val = "0"
                self.operations.append(
                    f"OP: Insert {insert_val} to {self.array} at {insert_pos}")
                self.init_elements(insert_val)
                t.destroy()
                return

            if insert_pos.get().lower() == "end":
                insert_pos = len(self.array)
            elif insert_pos.get().lower() == "begin":
                insert_pos = 0
            elif insert_pos.get().isdecimal():
                # If va elif co the thay insert_pos = len(self.array) thanh
                # #t.destroy() xong return de ngat function khi input > len hoac < 0
                if int(insert_pos.get()) > len(self.array):
                    mb.showinfo(
                        "Info", "Input is greater than array length. Set to end")
                    insert_pos = len(self.array)
                elif int(insert_pos.get()) < 0:
                    mb.showinfo(
                        "Info", "Position < 0. Set to end")
                    insert_pos = len(self.array)
                else:
                    insert_pos = int(insert_pos.get())
            else:
                # Tuong tu
                mb.showerror(
                    "Error", "Input neither \"End\" or decimal. Set to end")
                insert_pos = len(self.array)

            insert_val = sd.askstring("Value", "Enter a value to insert")

            if not insert_val:
                mb.showinfo("Info", "Value is empty. Set to length of array")
                insert_val = str(len(self.array))

            self.operations.append(
                f"OP: Insert {insert_val} to {self.array} at {insert_pos}")

            # Boi vi cac rect va text thuoc canvas nen dung method cua canvas lay vi tri cua rect va text
            prev_coord_rect = self.mainframe.coords(self.rects[insert_pos-1])
            prev_coord_text = self.mainframe.coords(self.texts[insert_pos-1])
            # Neu pos o cuoi thi them vao cuoi
            if not insert_pos < len(self.array):
                rect = self.mainframe.create_rectangle(
                    prev_coord_rect[0] + 80, prev_coord_rect[1], prev_coord_rect[2] + 80, prev_coord_rect[3])
                if len(insert_val) > 8:
                    mb.showinfo(
                        "Info", f"String length > 8. Discarded all character after {insert_val[7]}")
                    insert_val = insert_val[:4] + "\n" + insert_val[4:8]
                if 4 < len(insert_val) <= 8:
                    insert_val = insert_val[:4] + "\n" + insert_val[4:]
                text = self.mainframe.create_text(
                    prev_coord_text[0] + 80, prev_coord_text[1], text=f"{insert_val}", font=self.font)
                self.rects.insert(insert_pos, rect)
                self.texts.insert(insert_pos, text)
            # Neu pos khong o cuoi thi phai move cac rect va text khac ra sau truoc khi chen vao
            else:
                for i in reversed(range(insert_pos, len(self.array))):
                    self.mainframe.move(self.rects[i], 80, 0)
                    self.mainframe.move(self.texts[i], 80, 0)
                rect = self.mainframe.create_rectangle(
                    prev_coord_rect[0] + 80, prev_coord_rect[1], prev_coord_rect[2] + 80, prev_coord_rect[3])
                if len(insert_val) > 8:
                    mb.showinfo(
                        "Info", f"String length > 8. Discarded all character after {insert_val[7]}")
                    insert_val = insert_val[:4] + "\n" + insert_val[4:8]
                if 4 < len(insert_val) <= 8:
                    insert_val = insert_val[:4] + "\n" + insert_val[4:]
                text = self.mainframe.create_text(
                    prev_coord_text[0] + 80, prev_coord_text[1], text=f"{insert_val}", font=self.font)
                self.rects.insert(insert_pos, rect)
                self.texts.insert(insert_pos, text)

            # Neu them rect vao ma vuot qua width thi dieu chinh scrollregion de co the scroll sang phai/trai
            if self.rects:
                if self.mainframe.coords(self.rects[-1])[2] > WIN_WIDTH:
                    self.mainframe.config(scrollregion=(0, self.mainframe.coords(
                        self.rects[-1])[2] + 30, self.mainframe.coords(self.rects[-1])[2] + 30, 0))
            self.array.insert(insert_pos, insert_val)
            t.destroy()

        # Toplevel. Cai nay khong biet mieu ta kieu gi. Kieu them 1 window tk nua nhung phu thuoc vao root (root die thi top die)
        t = tk.Toplevel(self.root)
        # wm_transient loi top len truoc root
        t.wm_transient()
        t.title("Position")
        t.geometry(
            f"500x100+{int(t.winfo_screenwidth()/2 -250)}+{int(t.winfo_screenheight()/2 - 50)}")
        # Stringvar. Dung cho textvariable cua Entry. Khi nhap vao entry thi Stringvar cung se thay doi theo
        insert_pos = tk.StringVar(t, "")
        # Pack la 1 kieu dat widget khac. Pack se stack widget len nhau. Mac dinh center widget vao giua.
        tk.Label(t, text="Enter index to insert or \"End\" to insert to array end",
                 font=self.font).pack()
        insert_pos_entry = tk.Entry(t, textvariable=insert_pos, width=100)
        insert_pos_entry.pack()
        insert_pos_entry.focus()
        insert_pos_set = tk.Button(
            t, text="OK", font=self.font, command=lambda: insert_set(insert_pos))
        insert_pos_set.pack()

    def remove_element(self):
        # Phan tu sau t=tk.TopLevel(self.root) nhu function tren. Doan sau nhu tren nhung xoa di thay vi them vao
        def del_set():
            if del_pos.get().lower() == "end":
                rm_val = self.array.pop()
                self.operations.append(
                    f"OP: Removed {rm_val} from end of array")
                self.mainframe.delete(self.rects.pop())
                self.mainframe.delete(self.texts.pop())
                t.destroy()
            elif del_pos.get().lower() == "begin":
                rm_val = self.array.pop(0)
                self.operations.append(
                    f"OP: Removed {rm_val} from end of array.")
                self.mainframe.delete(self.rects.pop(0))
                self.mainframe.delete(self.texts.pop(0))
                t.destroy()
            elif del_pos.get().isdecimal():
                if int(del_pos.get()) > len(self.array):
                    mb.showinfo(
                        "Info", "Input is greater than array length. Set to end.")
                    rm_val = self.array.pop()
                    self.mainframe.delete(self.rects.pop())
                    self.mainframe.delete(self.texts.pop())
                    self.operations.append(
                        f"OP: Removed {rm_val} from end of array.")
                    t.destroy()
                elif int(del_pos.get()) < 0:
                    mb.showinfo(
                        "Info", "Input is lower than 0. Set to end.")
                    rm_val = self.array.pop()
                    self.mainframe.delete(self.rects.pop())
                    self.mainframe.delete(self.texts.pop())
                    self.operations.append(
                        f"OP: Removed {rm_val} from end of array.")
                    t.destroy()
                else:
                    rm_val = self.array.pop(int(del_pos.get()))
                    self.operations.append(
                        f"OP: Removed {rm_val} at {del_pos.get()}")
                    self.mainframe.delete(self.rects.pop(int(del_pos.get())))
                    self.mainframe.delete(self.texts.pop(int(del_pos.get())))
                    for i in range(int(del_pos.get()), len(self.array)):
                        self.mainframe.move(self.rects[i], -80, 0)
                        self.mainframe.move(self.texts[i], -80, 0)
                    t.destroy()
            else:
                mb.showerror(
                    "Error", "Input neither \"End\" or a number. Set to end")
                rm_val = self.array.pop()
                self.mainframe.delete(self.rects.pop())
                self.mainframe.delete(self.texts.pop())
                self.operations.append(
                    f"OP: Removed {rm_val} from end of array")
                t.destroy()
            t.destroy()

        if not self.array:
            return
        t = tk.Toplevel(self.root)
        t.wm_transient()
        t.title("Position")
        t.geometry(
            f"500x100+{int(t.winfo_screenwidth()/2 -250)}+{int(t.winfo_screenheight()/2 - 50)}")
        del_pos = tk.StringVar(t, "")
        tk.Label(t, text="Enter index to delete or \"End\" to delete last element",
                 font=self.font).pack()
        del_pos_entry = tk.Entry(t, textvariable=del_pos, width=100)
        del_pos_entry.pack()
        del_pos_entry.focus()
        del_pos_set = tk.Button(t, text="OK", font=self.font, command=del_set)
        del_pos_set.pack()

    def complete(self):
        def complete():
            t.destroy()
            self.root.destroy()
        t = tk.Toplevel(self.root)
        t.title("Operations")
        t.wm_transient()
        t.protocol("WM_DELETE_WINDOW", complete)
        t.geometry(
            f"600x600+{int(t.winfo_screenwidth()/2 -300)}+{int(t.winfo_screenheight()/2 -300)}")
        # Listbox: La 1 widget list cac item insert vao no. Co cac method nhu curselection, ... de lay selection trong listbox.
        # Giong nhu <input type="checkbox"> trong HTML
        listbox = tk.Listbox(t)
        for i in self.operations:
            listbox.insert(tk.END, i)
        listbox.place(x=0, y=50, width=600, height=500)
        button = tk.Button(t, text="Complete", font=self.font,
                           command=complete, bg="#ccc316", fg="#FFFFFF")
        button.pack()


if __name__ == "__main__":
    gui = GUI()
